# What should it be able to do

- Count days, weeks and months from a given date
- Show the current date
- show the initial date
- show a picture of the couple (changeable)
- change color theme

# What should it look like

For this part I created a mockup with [Figma](https://www.figma.com/).
I plan on updating this mockup as I go along with the project.
Later on I want to create color themes for the app, that you can choose from.

![Mockup](https://www.figma.com/file/JjZfPwWGqTlWixLk6PdU4S/Untitled?type=design&node-id=0%3A1&t=hMlEhyyWlR3OCNMs-1)

# Realising the project

I decided to use Flutter for this project, because I wanted to learn it for a while now and I thought this would be a nice project to start with.
I will use the [Flutter documentation](https://flutter.dev/docs) and [Stackoverflow](https://stackoverflow.com/) to help me with this project.

